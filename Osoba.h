#pragma once
#include<iostream>
#include"Data.h"

using namespace std;

class Osoba {
	string name;
	string surname;
	Data b_day;
	
	public:
	Osoba();
	Osoba(string name, string surname, Data);
	Osoba(string name, string surname, int day, int month, int year);
	//void B_day_1(int day, int month, int year);

	void display();

};

