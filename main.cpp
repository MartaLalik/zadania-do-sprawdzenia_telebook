#include<iostream>
#include"Data.h"
#include"Osoba.h"
#include"Kontakt.h"
#include"Adres.h"
#include"Firma.h"

using namespace std;

int main() {

	Data moja_data;
	moja_data.display();

	cout << "\n------------------------------------------------------------------------\n";

	int day, month, year;
	cout << "Podaj dzien: ";
	cin >> day;
	cout << "Podaj miesiac: ";
	cin >> month;
	cout << "Podaj rok: ";
	cin >> year;

	Data moja_data_1(day, month, year);
	moja_data_1.display();

	cout << "\n------------------------------------------------------------------------\n";

	Osoba moja_osoba;
	moja_osoba.display();

	cout << "\n------------------------------------------------------------------------\n";

	string name, surname;
	cout << "Podaj imie: ";
	cin >> name;
	cout << "Podaj nazwisko: ";
	cin >> surname;

	Data b_day;
	Osoba moja_osoba_1(name, surname, b_day);
	moja_osoba_1.display();

	cout << "\n------------------------------------------------------------------------\n";

	Kontakt moj_kontakt;
	moj_kontakt.display();

	cout << "\n------------------------------------------------------------------------\n";

	int cz_1, cz_2, cz_3;
	string email;
	cout << "Podaj nr telefonu: ";
	cin >> cz_1;
	cin >> cz_2;
	cin >> cz_3;
	cout << "Podaj email: ";
	cin >> email;

	Kontakt moj_kontakt_1(cz_1, cz_2, cz_3, email);
	moj_kontakt_1.display();

	cout << "\n------------------------------------------------------------------------\n";

	Adres moj_adres;
	moj_adres.display();

	cout << "\n------------------------------------------------------------------------\n";

	int nr_dom, nr_mieszkanie, kod;
	string ulica, miasto;
	cout << "Podaj nazwe ulicy: ";
	cin >> ulica;
	cout << "Podaj nr domu: ";
	cin >> nr_dom;
	cout << "Podaj nr mieszkania: ";
	cin >> nr_mieszkanie;
	cout << "Podaj kod pocztowy: ";
	cin >> kod;
	cout << "Podaj miasto: ";
	cin >> miasto;

	Adres moj_adres_1(nr_dom, nr_mieszkanie, kod, ulica, miasto);
	moj_adres_1.display();

	cout << "\n------------------------------------------------------------------------\n";

	Firma moja_firma;
	moja_firma.display();

	cout << "\n------------------------------------------------------------------------\n";

	string nazwa;
	cout << "Podaj nazwe firmy: ";
	cin >> nazwa;

	Adres miejsce;
	Kontakt kont;
	Data zatrudnienie;

	Firma moja_firma_1(nazwa, miejsce, kont, zatrudnienie);
	moja_firma_1.display();

	return 0;
}
