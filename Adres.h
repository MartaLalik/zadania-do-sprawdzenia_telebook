#pragma once
#include<iostream>

using namespace std;

class Adres {
	int nr_dom, nr_mieszkanie, kod;
	string ulica, miasto;

public:
	Adres();
	Adres(int nr_dom, int nr_mieszkanie, int kod, string ulica, string miasto);

	void display();

}; 
