#include<iostream>
#include "Adres.h"

using namespace std;

Adres::Adres() :
	nr_dom(11), nr_mieszkanie(95), kod(81063)
{
	cout << "Dziala konstruktor klasy Adres bez arg.\n";
	ulica = "Mlynska";
	miasto = "Gdynia";
}

Adres::Adres(int nr_dom, int nr_mieszkanie, int kod, string ulica, string miasto)
{
	cout << "Dziala konstruktor klasy Adres z arg.\n";
	this->ulica = ulica;
	this->nr_dom = nr_dom;
	this->nr_mieszkanie = nr_mieszkanie;
	this->kod = kod;
	this->miasto = miasto;
	}

void Adres::display()
{
	cout << "Adres: " << this->ulica << " " << this->nr_dom << "/" << this->nr_mieszkanie << endl;
	cout << this-> kod << " " << this->miasto << endl;
}
