#pragma once
#include<iostream>
#include "Data.h"

using namespace std;

Data::Data() :
	day(2), month(1), year(1900)
{
	cout << "Dziala konstruktor klasy Data bez arg.\n";
	//this->day = 1;
}

Data::Data(int day, int month, int year) :
	day(day), month(month), year(year)
{
	cout << "Dziala konstruktor klasy Data z arg.\n";
	this->day = day;

	if (day >= 1 and day <= 31) {
		this->day = day;
	}
	else {
		do {
			cout << "'Dzien' jest liczba mniejsza lub rowna 31 oraz wieksza lub rowna 1\n";
			cout << "Podaj prawidlowa wartosc\n";
			cin >> day;
		} while (day < 1 or day > 31);
		this->day = day;
	}

	this->month = month;

	if (month >= 1 and month <= 31) {
		this->month = month;
	}
	else {
		do {
			cout << "'Miesiac' jest liczba mniejsza lub rowna 12 oraz wieksza lub rowna 1\n";
			cout << "Podaj prawidlowa wartosc\n";
			cin >> month;
		} while (month < 1 or month > 12);
		this->month = month;
	}

	this->year = year;

	if (year >= 1900 and year <= 2023) {
		this->month = month;
	}
	else {
		do {
			cout << "'Rok' jest liczba mniejsza lub rowna 2023 oraz wieksza lub rowna 1900\n";
			cout << "Podaj prawidlowa wartosc\n";
			cin >> year;
		} while (year < 1900 or year > 2023);
		this->year = year;
	}

}

void Data::setDay(int day)
{
	this->day = day;
}

void Data::setMonth(int month)
{
	this->month = month;
}

void Data::setYear(int year)
{
	this->year = year;
}

int Data::getDay()
{
	return this->day;
}

int Data::getMonth()
{
	return this->month;
}

int Data::getYear()
{
	return this->year;
}

void Data::display()
{
	cout << "Data: " << this->day << "-" << this->month << "-" << this->year << endl;
}
