#include<iostream>
#include "Kontakt.h"

using namespace std;

Kontakt::Kontakt() :
    cz_1(662), cz_2(277), cz_3(387)
{
	cout << "Dziala konstruktor klasy Kontakt bez arg.\n";
	email = "martalalik@gmail.com";
}

Kontakt::Kontakt(int cz_1, int cz_2, int cz_3, string email) :
	cz_1(cz_1), cz_2(cz_2), cz_3(cz_3)
{
	cout << "Dziala konstruktor klasy Kontakt z arg.\n";
	this->cz_1 = cz_1;
	this->cz_2 = cz_2;
	this->cz_3 = cz_3;
	this->email = email;
}

void Kontakt::setCz_1(int cz_1)
{
	this->cz_1 = cz_1;
}

void Kontakt::setCz_2(int cz_2)
{
	this->cz_2 = cz_2;
}

void Kontakt::setCz_3(int cz_3)
{
	this->cz_3 = cz_3;
}

int Kontakt::getCz_1()
{
	return this->cz_1;
}

int Kontakt::getCz_2()
{
	return this->cz_2;
}

int Kontakt::getCz_3()
{
	return this->cz_3;
}

void Kontakt::display()
{
	cout << "Nr telefonu: " << this->cz_1 << "-" << this->cz_2 << "-" << this->cz_3 << endl;
	cout << "Email: " << this->email;
}
