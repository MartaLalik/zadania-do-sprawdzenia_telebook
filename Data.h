#pragma once

class Data {
	int day, month, year;
public:
	Data();
	Data(int day, int month, int year);

	void setDay(int);
	void setMonth(int);
	void setYear(int);

	int getDay();
	int getMonth();
	int getYear();

	void display();
};

