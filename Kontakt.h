#pragma once
#include<iostream>

using namespace std;

class Kontakt {
	int cz_1, cz_2, cz_3;
	string email;
	
public:
	Kontakt();
	Kontakt(int cz_1, int cz_2, int cz_3, string email);

	void setCz_1(int);
	void setCz_2(int);
	void setCz_3(int);

	int getCz_1();
	int getCz_2();
	int getCz_3();
		
	void display();

}; 
