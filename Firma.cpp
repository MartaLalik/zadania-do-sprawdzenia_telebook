#include<iostream>
#include "Firma.h"

using namespace std;

Firma::Firma()
{
	cout << "Dziala konstruktor klasy Firma bez arg.\n";
	nazwa = "Szkola";
}

Firma::Firma(string nazwa, Adres, Kontakt, Data)
{
	cout << "Dziala konstruktor klasy Firma z arg.\n";
	this->nazwa = nazwa;
	
}

void Firma::display()
{
	cout << "Nazwa: " << this->nazwa << " " << endl;
	this->miejsce.display();
	this->kont.display();
	this->zatrudnienie.display();
}

