#pragma once
#include<iostream>
#include"Data.h"
#include"Adres.h"
#include"Kontakt.h"

using namespace std;

class Firma {
	string nazwa;
	Adres miejsce;
	Kontakt kont;
	Data zatrudnienie;
	
public:
	Firma();
	Firma(string nazwa, Adres, Kontakt, Data);
	//void B_day_1(int day, int month, int year);

	void display();

};

